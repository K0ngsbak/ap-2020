//Posts with comments that will be translated
var post;
//Button styled with CSS for exam project
var button1, button2, button3, button4, button5, button6, button7, button8, button9, button10;

function preload() {
  img1 = loadImage('fbLogo.png');
  post = loadJSON("post.JSON");
}

function setup() {
  createCanvas(800,1800);

  // facebook original bg color
  background(255);

  fill(233,235,238);
  strokeWeight(0.2);
  rect(200,0,600,1800);

  fill(61,106,214);
  rect(200,0,600,40);
  image(img1,190,-1.5,60,45);


  // post 1
  fill(255);
  strokeWeight(0.1);
  rect(300,100,400,185,3)

  // pfp and name
  fill(170,201,255);
  circle(330,135,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 1',355,135);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('7th of October 3:33pm',355,150);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[0], 320, 165, 350, 100);

  // post 2
  fill(255);
  strokeWeight(0.1);
  rect(300,300,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,330,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 2',355,330);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('5th of November 10:45am',355,344);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[1], 320, 365, 350, 100);

  // post 3
  fill(255);
  strokeWeight(0.1);
  rect(300,445,400,155,3);

  // pfp and name
  fill(170,201,255);
  circle(330,475,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 3',355,475);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('12th of November 07:04pm',355,490);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[2], 320, 510, 350, 100);

  // post 4
  fill(255);
  strokeWeight(0.1);
  rect(300,615,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,645,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 4',355,645);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('31th of December 11:55pm',355,660);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[3], 320, 680, 350, 100);

  // post 5
  fill(255);
  strokeWeight(0.1);
  rect(300,760,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,790,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 5',355,790);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('5th of January 05:46pm',355,805);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[4], 320, 820, 350, 100);

  // post 6
  fill(255);
  strokeWeight(0.1);
  rect(300,905,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,935,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 6',355,935);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('16th of February 1:09am',355,950);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[5], 320, 965, 350, 100);


  // post 7
  fill(255);
  strokeWeight(0.1);
  rect(300,1055,400,200,3)

  // pfp and name
  fill(170,201,255);
  circle(330,1085,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 7',355,1085);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('28th of February 12:32pm',355,1100);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[6], 320, 1115, 350, 110);


  // post 8
  fill(255);
  strokeWeight(0.1);
  rect(300,1275,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,1305,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 8',355,1305);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('2nd of March 02:47am',355,1320);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[7], 320, 1340, 350, 100);

  // post 9
  fill(255);
  strokeWeight(0.1);
  rect(300,1425,400,130,3)

  // pfp and name
  fill(170,201,255);
  circle(330,1455,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 9',355,1455);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('15th of March 10:14pm',355,1470);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[8], 320, 1490, 350, 100);

  // post 10
  fill(255);
  strokeWeight(0.1);
  rect(300,1575,400,150,3)

  // pfp and name
  fill(170,201,255);
  circle(330,1605,37);
  fill('#2851A3');
  textStyle(BOLD);
  textSize(15);
  text('User 10',355,1605);
  fill('#828282');
  textSize(13);
  textStyle(NORMAL);
  text('1st of April 09:27am',355,1620);

  // calling text from JSON
  fill(50);
  textSize(15);
  text(post.posts[9], 320, 1640, 350, 100);

  // no more posts
  textStyle(NORMAL);
  textSize(17);
  fill(190);
  text('There are no more posts to show right now.',330,1770);
}

function draw() {
  // buttons
  push();
  button1=createButton('See Emotional Translation');
  button1.style("color","#17397F");
  button1.style("font-size","0.9em");
  button1.style("display","inline-block");
  button1.style("background-color","#FFFFFF");
  button1.style("padding","2px 4px");
  button1.style("border-radius:8px");
  button1.style("border","none");
  button1.position(315,245);
  button1.mousePressed(translateComment1);

  button2=createButton('See Emotional Translation');
  button2.style("color","#17397F");
  button2.style("font-size","0.9em");
  button2.style("display","inline-block");
  button2.style("background-color","#FFFFFF");
  button2.style("padding","2px 4px");
  button2.style("border-radius:8px");
  button2.style("border","none");
  button2.position(315,395);
  button2.mousePressed(translateComment2);

  button3=createButton('See Emotional Translation');
  button3.style("color","#17397F");
  button3.style("font-size","0.9em");
  button3.style("display","inline-block");
  button3.style("background-color","#FFFFFF");
  button3.style("padding","2px 4px");
  button3.style("border-radius:8px");
  button3.style("border","none");
  button3.position(315,570);
  button3.mousePressed(translateComment3);

  button4=createButton('See Emotional Translation');
  button4.style("color","#17397F");
  button4.style("font-size","0.9em");
  button4.style("display","inline-block");
  button4.style("background-color","#FFFFFF");
  button4.style("padding","2px 4px");
  button4.style("border-radius:8px");
  button4.style("border","none");
  button4.position(315,715);
  button4.mousePressed(translateComment4);

  button5=createButton('See Emotional Translation');
  button5.style("color","#17397F");
  button5.style("font-size","0.9em");
  button5.style("display","inline-block");
  button5.style("background-color","#FFFFFF");
  button5.style("padding","2px 4px");
  button5.style("border-radius:8px");
  button5.style("border","none");
  button5.position(315,855);
  button5.mousePressed(translateComment5);

  button6=createButton('See Emotional Translation');
  button6.style("color","#17397F");
  button6.style("font-size","0.9em");
  button6.style("display","inline-block");
  button6.style("background-color","#FFFFFF");
  button6.style("padding","2px 4px");
  button6.style("border-radius:8px");
  button6.style("border","none");
  button6.position(315,1012);
  button6.mousePressed(translateComment6);

  button7=createButton('See Emotional Translation');
  button7.style("color","#17397F");
  button7.style("font-size","0.9em");
  button7.style("display","inline-block");
  button7.style("background-color","#FFFFFF");
  button7.style("padding","2px 4px");
  button7.style("border-radius:8px");
  button7.style("border","none");
  button7.position(315,1230);
  button7.mousePressed(translateComment7);

  button8=createButton('See Emotional Translation');
  button8.style("color","#17397F");
  button8.style("font-size","0.9em");
  button8.style("display","inline-block");
  button8.style("background-color","#FFFFFF");
  button8.style("padding","2px 4px");
  button8.style("border-radius:8px");
  button8.style("border","none");
  button8.position(315,1370);
  button8.mousePressed(translateComment8);

  button9=createButton('See Emotional Translation');
  button9.style("color","#17397F");
  button9.style("font-size","0.9em");
  button9.style("display","inline-block");
  button9.style("background-color","#FFFFFF");
  button9.style("padding","2px 4px");
  button9.style("border-radius:8px");
  button9.style("border","none");
  button9.position(315,1525);
  button9.mousePressed(translateComment9);

  button10=createButton('See Emotional Translation');
  button10.style("color","#17397F");
  button10.style("font-size","0.9em");
  button10.style("display","inline-block");
  button10.style("background-color","#FFFFFF");
  button10.style("padding","2px 4px");
  button10.style("border-radius:8px");
  button10.style("border","none");
  button10.position(315,1690);
  button10.mousePressed(translateComment10);
  pop();
}

function translateComment1() {
  fill(255);
  strokeWeight(0);
  rect(315,155,385,82,3)
  fill(50);
  textSize(15);
  text(post.translations[0], 320, 165, 350, 100);
}


function translateComment2() {
  fill(255);
  strokeWeight(0);
  rect(300,350,395,60,3)
  fill(50);
  textSize(15);
  text(post.translations[1], 320, 355, 350, 100);
}


function translateComment3() {
  fill(255);
  strokeWeight(0);
  rect(300,500,400,70,3);
  fill(50);
  textSize(15);
  text(post.translations[2], 320, 520, 350, 100);
}

function translateComment4() {
  fill(255);
  strokeWeight(0);
  rect(300,670,400,80,3)
  fill(50);
  textSize(15);
  text(post.translations[3], 320, 670, 350, 100);
}

function translateComment5() {
  fill(255);
  strokeWeight(0);
  rect(300,810,400,80,3)
  fill(50);
  textSize(15);
  text(post.translations[4], 320, 820, 350, 95);
}

function translateComment6() {
  fill(255);
  strokeWeight(0);
  rect(300,955,400,95,3)

  fill(50);
  textSize(15);
  text(post.translations[5], 320, 960, 350, 60);
}

function translateComment7() {
  fill(255);
  strokeWeight(0);
  rect(300,1110,400,120,3)

  fill(50);
  textSize(15);
  text(post.translations[6], 320, 1150, 350, 100);
}

function translateComment8() {

  fill(255);
  strokeWeight(0);
  rect(300,1333,400,60,3)

  fill(50);
  textSize(15);
  text(post.translations[7], 320, 1333, 350, 100);
}

function translateComment9() {
  fill(255);
  strokeWeight(0);
  rect(300,1470,400,70,3)
  fill(50);
  textSize(15);
  text(post.translations[8], 320, 1490, 350, 90);
}

function translateComment10() {
  fill(255);
  strokeWeight(0);
  rect(300,1630,400,60,3);
  fill(50);
  textSize(15);
  text(post.translations[9], 320, 1640, 350, 100);
}
