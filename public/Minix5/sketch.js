var button;
var bgcolor;

function changeColor(){
  bgcolor=color(random(255),0,random(255));
}
function setup() {
  // put setup code here
  print("hello world");
  createCanvas(1700,1000);
  //giving the bgcolor variable a value
  bgcolor=color(0,190,250);
  //Styling the buttons with CSS
  //This-button
  button=createButton('THIS BUTTON');
  button.style("font-size","1em");
  button.style("display","inline-block");
  button.style("background-color","rgb(140,85,60)");
  button.style("padding","12px 28px");
  button.style("border-radius:8px");
  button.style("border","none");
  button.position(880,885);
  //Changing the background color with button
  button.mousePressed(changeColor);
  }
function draw() {
  background(bgcolor);
  //Question for user
  fill(0,0,0);
  noStroke();
  textSize(60);
  textStyle(BOLD);
  textAlign(CENTER);
  text('LET ME FORESEE YOUR FUTURE SELF-ESTEEM',850,100);
  //Drawing the crystal ball
  frameRate(10);
  fill(130,75,50);
  beginShape();
  vertex(575,702);
  vertex(680,789);
  vertex(789,824);
  vertex(900,828);
  vertex(1000,799);
  vertex(1125,702);
  vertex(1125,780);
  vertex(1180,980);
  vertex(525,980);
  vertex(575,780);
  endShape();
  noFill();
  strokeWeight(10);
  stroke(0,0,255);
  ellipse(850,480,700,700);
  //Text on the crystal
  noStroke();
  fill(0,0,0);
  textSize(24);
  text("If you want to know how you feel about yourself",850,868);
  text("in the future press",755,915);
  textSize(19);
  text("(Remember that the future depends on what you do and do not do)",850,960);
  //The invisible line between the good and the bad future
  fill(0,190,250);
  beginShape();
  vertex(851,135);
  vertex(851,826);
  vertex(849,135);
  vertex(849,826);
  endShape();
  //The bad future = worse self-esteem
  textSize(30);
  text("THIS IS THE BAD FUTURE:",230,170);
  textSize(18);
  textStyle(ITALIC);
  text("IF YOU DO NOT FIGHT FOR A BETTER",210,210);
  text("SELF-ESTEEM THEN THIS IS HOW YOU",210,245);
  text("ARE GOING TO FEEL ABOUT YOURSELF...",220,280);
  //Negative words
  textStyle(BOLD);
  textSize(24);
  text("broken",790,400);
  text("weak",790,170);
  text("worthless",750,280);
  text("sad",720,225);
  text("angry",770,789);
  text("empty",750,700);
  text("depressed",650,578);
  text("alone",550,500);
  text("lonely",590,400);
  text("sorry",710,750);
  text("needy",780,525);
  text("dysfunctional",700,450);
  text("self-destructive",650,650);
  text("ugly",610,320);
  text("confussed",780,615);
  text("not enough",700,350)
  //The good future = better self-esteem
  textSize(30);
  text("THIS IS THE GOOD FUTURE:",1400,170);
  textSize(18);
  textStyle(ITALIC);
  text("IF YOU DO FIGHT FOR A BETTER",1350,210);
  text("SELF-ESTEEM THEN THIS IS HOW YOU",1370,245);
  text("ARE GOING TO FEEL ABOUT YOURSELF...",1380,280);
  //positive words
  textStyle(BOLD);
  textSize(24);
  text("beautiful",1120,400);
  text("happy",900,170);
  text("creative",1050,280);
  text("worthy of love",950,225);
  text("accepted",930,789);
  text("cool",1050,700);
  text("friendly",940,578);
  text("funny",934,500);
  text("brave",1140,520);
  text("loyal",940,650);
  text("honest",1080,585);
  text("skilled",930,400);
  text("smart",930,730);
  text("effective",1080,470);
  text("sexy",1120,645);
  text("more than enough",970,335)
    }
