**Warning: I went with “low floor” this time due to lack of time and a bunch of personal issues that came out of nowhere**


**Runme:**


Link to sketch: https://k0ngsbak.gitlab.io/ap-2020/Minix5/

Link to minix3: https://k0ngsbak.gitlab.io/ap-2020/Minix3/

Link to minix4: https://k0ngsbak.gitlab.io/ap-2020/Minix4/

Link to repository: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public%2FMinix5

Screenshot:
![Screenshot](loadingselfesteempart2.PNG)



**Background:** 

I have chosen to revisit my minix3 conceptually and revisit my minix4 technically. For my minix4 I worked with hidden text and a button that could change the color of the background. For my minix3 I wanted to criticize an issue that I have experienced myself as to why I thought about a bunch of personal issues such as; sexual harassment, low self-esteem, bullying, anxiety, depression etc. Afterwards, I settled on “low self-esteem” and thought of how I used to believe that my self-esteem would get better with time and all I had to do was wait. Spoiler alert; Doing nothing but waiting isn’t working. This belief let to a bunch of other issues, horrible relationships and a self-destructive way of thinking because of how it didn’t get better, it got worse. So, what I mainly want to express is that self-esteem is something you work for, not wait for. Furthermore, I want to express how self-destructive low self-esteem can be, how “I’m fine” doesn’t always hide the tears/how tears cuts through your “I’m fine” and what “I’m fine’ often tries to hide. 


**The concept**

So, for this minix I wanted to stay true to my message of how self-esteem is something you work for, not wait for – but in a more… Positive… way. I chose to design a crystal ball that could foresee a person’s future self-esteem with two possible outcomes; A positive outcome and a negative outcome depending on whether this person is going to work for a better self-esteem. 


**What changes and why**

Thinking of my minix3 I have changed the mood of the design and made it interactive. Further, I have changed a bunch of smaller and simple things such as the background color and use of shapes. Thinking of my minix4 I have changed the design of the button, but that’s about it. I chose to change the mood of the design based on how I wanted test the impact of the mood of the design. I chose to make the button blend in with the design compared to the button in my minix4 because of how I did not want the button to take away the focus due to how the message in this minix is much more important than the button. 


**Bonus**

I might re-do this minix in the future when I have more time and aren’t having an hour-long anxiety attack every day. If I am going to re-do this minix I think that I would have the crystal ball change color and not the whole background. Further, I would make an array of the negative words and an array of the positive words so that when the user clicks “this button” only one word will appear on each side of the crystal. I might even load a song to play in the background.  

