//Note to self and user: Remember to view the program in a fully opened window.
//Note to self and user: If the program is missing - resize the window.
var button;
var bgcolor;
function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
function changeColor(){
  bgcolor=color(random(255),random(255),random(255));
}

function setup() {
  // put setup code here
  print("hello world");
  //giving the bgcolor variable a value
  bgcolor=color(255,0,0);
  //Styling the buttons with CSS
  //Yes-button
  button=createButton('YES');
  button.style("font-size","2em");
  button.style("display","inline-block");
  button.style("background-color","#4CAF50");
  button.style("padding","24px 56px");
  button.style("border-radius:8px");
  button.style("border","none");
  button.position(50,125);
  //Changing the background color with button
  button.mousePressed(changeColor);
  }
function draw() {
  background(bgcolor);
  //Question for user
  fill(0,0,0);
  textSize(60);
  textStyle(BOLD);
  textAlign(CENTER);
  text('ARE YOU READY FOR SOME COMPLIMENTS?',720,100);
  //Drawing the im-perfect heart
  frameRate(10);
  fill(random(255),random(255),random(255));
  noStroke();
  ellipse(625,300,250,245);
  ellipse(825,300,250,245);
  triangle(510,350,940,350,730,575);
  //Placing the "secret" compliments for the user
  fill(255,0,0);
  textSize(24);
  text("Hey beautiful",200,250);
  text("How are you doing handsome?",200,350);
  text("You are an awesome friend",200,450);
  text("You are one smart cookie",200,550);
  text("I like your style",200,625);
  text("You have such nice eyes",200,700);
  text("I love your smile!",450,150);
  text("you are more than good enough!",600,700);
  text("You should be proud of yourself",800,150);
  text("I like your sense of humor",700,950);
  text("On a scale from 1 to 10 you are an 11",1200,250);
  text("You are strong!",530,525);
  text("You bring out the best in people",1200,350);
  text("I bet you sweat glitter",1100,450);
  text("You are better than a triple-scoop ice cream cone. With sprinkles",950,625);
  text("You are someone's reason to smile",1100,525);
  text("I believe in you!",1100,700);
    }
