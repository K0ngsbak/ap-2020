***(SCRATCH) SELF HARM***

**IMPORTANT: Use the program before reading the readme.**

**IMPORTANT: View the program in a fully opened window – if at first the program will not appear, please re-size the window.** 


***RUNME:***

**Link to the program:** https://k0ngsbak.gitlab.io/ap-2020/Minix4/ 

**Link to repository:** https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public%2FMinix4 


***README:***

**Thoughts before coding:**
-	In our daily lives likes matter a lot; often a lot more than any of us would like (pun intended). We compare ourselves to each other and we get sad about the fact that we don't have that body, that smile, that eye color or are as happy as the person in the picture - we compare ourselves to everyone else, instead of learning to love who we are for what we are. My design addresses this problem by focusing on self-love as to why I wanted the camera to turn on when the user clicked “yes” to “Are you ready for some compliments?”, however due to lack of skills this was not possible as to why I changed it into the following: When the user clicks the “yes”-button, the background color will change and a bunch of compliments will appear around the “blinking” heart on the screen. This is supposed to illustrate the importance of loving oneself. 

**Title and description:**
-	My program is called “(SCRATCH) SELF HARM” and it tries to illustrate the importance of self-love in a time, where we tend to criticize ourselves for what we are not instead of complimenting ourselves for who we are (I too am guilty of this). The program asks the user “are you ready for some compliments?” which of course is a “yes”/”no” kind of question, however it is not possible to answer the question with “no”. There are to (at least) reasons for this. The first reason being: if you want to answer this question with no, then you probably need the yes-outcome more than you think. The second reason being: the program tries to illustrate both the importance of self-love but also how technology impacts our lives a lot more than most of us notice. We tend to believe that we are in control of everything that happens online when often are not; You don’t even have the control to answer a simple yes/no question. 
-   My hopes are that this last reason will make the user think about what he/she is in control of and not about what he/she is not in control of because of how I believe that you are in control of your own self-harm and your own self-love. In other words, I want the user to stop criticizing him-/herself for what he/she is not and start loving him-/herself for what he/she is. 

**Describe your program and what you have used and learnt.** 
-	My program consist of: an imperfect heart “blinking” in different colors, a “yes”-button that makes the background color change and a bunch of hidden compliments. The heart is imperfect because of how all of us have our own imperfections as to why the hearts serves as a reminder to the user. The heart is “blinking” in different colors to illustrate how different people are from each other as to why we should not compare ourselves to other people; and sadly, this is easier said than done. 
-   In my program I have done quite a few things for the first time; I have created my own variables for the first time, I have used the window resize function for the first time, I have used “random” for the first time, I have used mousePressed for the first time and I have created a button for the first time. At this point, most of these are familiarities for most of the class, however I have been to “scared” to use them until now. 
-   In my program, I have also used syntaxes, that I am familiar with such as: fill, text size, text style, text, framerate, noStroke, ellipse and triangle. 
  

**SCREENSHOTS**

**Screenshot 1:** ![Screenshot](scratchselfharm1.png)

**Screenshot 2:** ![Screenshot](scratchselfharm2.png)
