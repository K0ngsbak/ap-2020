//Created by group 4: Sofie, Mira, Malene & Emma
//April 5th 2020

// Variables
var api= "http://api.giphy.com/v1/gifs/search?";
var apiKey = "&api_key=LZRiEqvscrnUxtHIKaGde4w8aAFKdOyb&q=";
var query; // There's more than one query, they are called in the functions
var button1; //Button for angry
var button2; //Button for happy
var button3; //Button for sad
var button4; //Button for frustruated
var button5; //Button for tired
var button6; //Button for excited
var button7; //Button for in love
var button8; //Button for scared

// Preload for the headline font
function preload() {
  fontBlackbird = loadFont('Blackbird.ttf');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  // Creating and styling the angy-button
  button1 = createButton('angry');
  button1.size(75,75);
  button1.style("font-weight", "bold");
  button1.style("color", "#6600CC");
  button1.style("background-color", "white");
  button1.position(100,150);
  button1.mousePressed(angry);

  // Creating and styling the happy-button
  button2 = createButton('happy');
  button2.size(75,75);
  button2.style("font-weight", "bold");
  button2.style("color", "#6600CC");
  button2.style("background-color", "white");
  button2.position(200,150);
  button2.mousePressed(happy);

  // Creating and styling the sad-button
  button3 = createButton ('sad');
  button3.size(75,75);
  button3.style("font-weight", "bold");
  button3.style("color", "#6600CC");
  button3.style("background-color", "white");
  button3.mousePressed(sad);
  button3.position(300,150);

  // Creating and styling the frustrated-button
  button4 = createButton('frustrated');
  button4.size(75,75);
  button4.style("font-weight", "bold");
  button4.style("color", "#6600CC");
  button4.style("background-color", "white");
  button4.position(400,150);
  button4.mousePressed(frustrated);

  // Creating and styling the tired-button
  button5 = createButton('tired');
  button5.size(75,75);
  button5.style("font-weight", "bold");
  button5.style("color", "#6600CC");
  button5.style("background-color", "white");
  button5.position(100,250);
  button5.mousePressed(tired);

  // Creating and styling the excited-button
  button6 = createButton('excited');
  button6.size(75,75);
  button6.style("font-weight", "bold");
  button6.style("color", "#6600CC");
  button6.style("background-color", "white");
  button6.position(200,250);
  button6.mousePressed(excited);

  // Creating and styling the in love-button
  button7 = createButton('in love');
  button7.size(75,75);
  button7.style("font-weight", "bold");
  button7.style("color", "#6600CC");
  button7.style("background-color", "white");
  button7.position(300,250);
  button7.mousePressed(love);

  //Creating and styling the scared-button
  button8 = createButton('scared');
  button8.size(75,75);
  button8.style("font-weight", "bold");
  button8.style("color", "#6600CC");
  button8.style("background-color", "white");
  button8.position(400,250);
  button8.mousePressed(scared);
}

function draw() {
  // Headline question for the user
  background(204,204,255);
  fill(76,9,153);
  textSize(40);
  textFont(fontBlackbird);
  text('How are you feeling today?', 92, 102);
  fill(178,102,255);
  text('How are you feeling today?', 90, 100);
}


function gotData(giphy) {
  // Makes the gif appear, and chooses randomly from the array of gifs.
  let img = createImg(giphy.data[Math.floor(Math.random()*query.length)].images.original.url);
  // Changes the position of the gif every time a button is pressed to visualize scattered feelings
  img.position(random(400, windowWidth+400), random(150, windowHeight-130));
}

// The following functions determine the searchword(s) for the gif,
// depending on which button is pressed, and then collects the right data from the API
function angry () {
  query = "angry"
  var url = api + apiKey + query;
  giphy =  loadJSON(url, gotData);
}

function happy() {
  query = "happy"
  var url = api + apiKey + query;
  giphy = loadJSON(url, gotData);
}

function sad() {
  query = "sad"
  var url = api + apiKey + query;
  giphy = loadJSON(url, gotData);
}

function frustrated() {
  query = "frustrated"
  var url = api + apiKey + query;
  giphy = loadJSON(url,gotData);
}

function tired() {
  query = "tired"
  var url = api + apiKey + query;
  giphy = loadJSON(url,gotData);
}

function excited() {
  query = "excited"
  var url = api + apiKey + query;
  giphy = loadJSON(url,gotData);
}

function love() {
  query = "in+love"
  var url = api + apiKey + query;
  giphy = loadJSON(url,gotData);
}

function scared() {
  query = "scared"
  var url = api + apiKey + query;
  giphy = loadJSON(url,gotData);
}
