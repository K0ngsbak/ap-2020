**README**

In my program I have used; the fill syntax, the vertex syntax, the image syntax,
the ellipse syntax, the rectangle syntax, the triangle syntax, the stroke 
syntax, the no stroke syntax, the stroke weight syntax, the text syntax, the 
text style syntax and the text size syntax. My program consists of a protest 
emoji and a friendzone emoji and I put both into a context where one could use
these two emojis. The protest emoji is the emoji on the left side if the canvas
and it is supposed to be both gender-mutual and race-mutual because of the fact
that no matter who you are, you can have different opinions and therefore fight
for / protest against different things. The friendzone emoji is the emoji on 
the right side of the canvas. The emoji is supposed to illustrate how two people
who care deeply for each other can have completely opposite feelings for each 
other due to how one can want a friendship and how the other person might want 
more. This is an alternative to the broken-heart emoji which in my opinion can
seem as the extreme; you are not heartbroken just because you’re crushing on
someone who doesn’t feel the same way. I would like to believe, that the friend-
zone emoji is a way of saying “I like you and it is okay that you do not feel 
the same way”. 

**RUNME**
•	Link: www.k0ngsbak.gitlab.io/ap-2020/Minix2/

•	Link: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix2 

•	Screenshot: ![Screenshot](screenshotminix2.jpg)
