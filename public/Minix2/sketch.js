let chatsofie;
function preload(){
  chatsofie = loadImage('chatsofie.JPG');
}
function setup() {
  // put setup code here
  createCanvas(700,700);
  print("hello world");
  background(chatsofie);
}
function draw() {
  // put drawing code here
  //title nr 1
  fill(255,0,0);
  textStyle(BOLD);
  textSize(16);
  text('Emoji nr. 1: The protest emoji',30,570);
  //shirt
  noStroke();
  fill(0,0,0);
  rect(50,430,100,100);
  //neck
  noStroke();
  fill(255,220,0);
  triangle(80,415,120,415,100,470)
  //R,G,& B integer values
  stroke(255,200,0);
  strokeWeight(5);
  //'yellow' color for face
  fill(255,220,0)
  circle(100,370,100);
  //eye on the right
  fill(255,255,255);
  circle(122,359,30);
  //eye on the left
  circle(80,359,30);
  //eye color, blue
  noStroke();
  fill(0,150,255);
  circle(80,360,15);
  circle(122,360,15);
  //pupils, black
  fill(0,0,0);
  circle(80,360,7);
  circle(122,360,7);
  //Making the eyes angry-looking
  beginShape();
  vertex(107,344);
  vertex(139,334);
  vertex(139,341);
  vertex(107,351);
  endShape(CLOSE);

  beginShape();
  vertex(64,334);
  vertex(96,344);
  vertex(96,351);
  vertex(64,341);
  endShape(CLOSE);

  //mouth
  rect(75,385,50,20);
  //white text
  fill(255,255,255);
  textSize(14);
  text('&@!#%',76,400);

  //left arm
  fill(0,0,0);
  beginShape();
  vertex(50,430);
  vertex(50,470);
  vertex(25,510);
  vertex(25,475);
  endShape(CLOSE);

  beginShape();
  vertex(25,475);
  vertex(50,500);
  vertex(50,530);
  vertex(25,510);
  endShape(CLOSE);
  //right arm
  beginShape();
  vertex(150,430);
  vertex(150,470);
  vertex(175,395);
  vertex(175,355);
  endShape(CLOSE);

  //stick for protest sign
  fill(184,92,0);
  rect(170,339,15,90);
  //protest sign
  fill(200,200,200,5);
  rect(105,265,150,75);
  fill(255,0,0);
  textStyle(BOLD);
  textSize(85);
  text('!',164,332);

  //title nr 2
  fill(0,0,255);
  textStyle(BOLD);
  textSize(16);
  text('Emoji nr. 2: The friendzone emoji',390,570);
  // white circle
  fill(255,255,255);
  stroke(0,0,0);
  circle(510,395,175);
  //heart for the in love part
  noStroke();
  fill(200,0,0,);
  circle(510,375,50);
  circle(550,375,50);
  triangle(490,390,570,390,530,425);
  //hand for the frind part
  fill(0,0,200);
  rect(435,380,25,50,10);
  rect(465,380,50,50,10);
  rect(465,360,15,35,10);
  //note to self
  fill(0,255,0,4);
  rect(178,420,270,90)
  //title nr 3
  fill(0,0,0);
  textStyle(ITALIC);
  textSize(16);
  text('A situation where you can use',188,440);
  text('both of these emojis; emoji nr. 1',188,460);
  text('being: The protest emoji and emoji',188,480);
  text('nr. 2 being: The friendzone emoji',188,500);
}
