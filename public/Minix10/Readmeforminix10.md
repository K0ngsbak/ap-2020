## Individual
Link to repository: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix10

Screenshot: ![Screenshot](PONGflowchart.png)


**Process of making the flowchart**
For this minix I have chosen to make a flowchart of my Pong-game (minix6: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix6) for several reasons. First, this was the most technically complex one for me to work with. Secondly, I probably would have liked to have a flowchart when making the game, because it turned out to be quite complex to work with quite fast – and I struggled a lot. Funny thing is, when making the flowchart this time around, I thought that it would be easy to do when I could just look at the code and make it based on the code. That didn’t work out for me since I quickly realized that when I made the code, I jumped back and forth – so I couldn’t just look at the code line by line because that was not how I made the game in the first place. That meant closing my computer and finding pen and paper instead – in other words; starting from scratch. I asked myself: “How to you want to make the flowchart?” and I wrote down: “A help from my future self to my past self”. In order to do so, I tried to think of how I made it first time around – but this did not work for me either. Then what? Well, I guess third time is a charm. When I didn’t succeed, I decided that I would pretend like I didn’t know how to make the game and try to make the flowchart as if I was going to make it for the first time. I think this worked out great since this way of thinking was how I made the final flowchart. 
Personally, this minix have taught me (among others) that I prefer to make the flowchart before the code. 


**What is the value of the individual and the group flowchart that you have produced?**
I have approached my individual flowchart the same way that we approached the group flowchart by making the flowcharts make sense to myself/ourselves. The value of flowcharts for me is based on the fact that it is a huge help in terms of communicating; both the conceptual and technical aspects of a program – especially when working in a group since it helps us reach a common understanding of our ideas. It is, however, important for me to state that I do not see flowcharts as some fort of final walkthrough of an idea, but rather as an overview of the basics; a blueprint some might say. On that note, I think that it is important to look at the flowchart again during the process of making the code of the program in terms of how it can help you stay on track – and maybe help you out if you get lost. In other words; there is (to me) a value in flowcharts that I did not realize before this minix – it is not just something you make in the beginning and then never looks at again, it is a way of understanding an idea and getting more concrete first time around – at least to me it is. 

## Group
#### Who are you collaborating with / which group?
**Group 4:** Sofie Berg Pedersen @SofieBP , Mira Bella Dyring Morsø @MiraMorso, Malene Storm Blomgren @maleneblomgren & Emma Marie Kongsbak Bertelsen @K0ngsbak

**Our ideas**
*ET (Emotional Translator):*
The overall idea for this is to make a look similar to the translate feature we see on Facebook. On Facebook you have the option to get comments and posts translated into your own language if the text is in a foreign language to you. It gives the user the ability to understand all content shared, even if you don’t understand the language it’s written in. We wanted to play with the idea of the facade we put on when sharing our lives on social media, and create an “Emotional Translator,” that will give the user the option to understand what the author of the post or comment is really thinking or feeling.

Screenshot: ![Screenshot](ETflowchart.jpg)

*PS (Personal Spindoctor):*
The overall idea for this is to make an somewhat nihilistic AI that can/will help the user achieve the best (and possibly the most honest) post to post on a social media platform. We wanted to comment on how many users on social media post their best selves and often not their honest selves. We find that social media accounts are often putting up a façade as to why we acknowledge why some might criticize the lack of “deepness” in some posts on social media. 

Screenshot: ![Screenshot](PSflowchart.PNG)

**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**
Some things are easier to express when using the correct term, for example array, preload, setup, draw and so forth, yet people who are unfamiliar with the language will not understand the words. It takes some time, to convert your idea into common tongue, so that it can be a proper communication device, that allows a diverse team of people to corporate. 
At another level of complexity is also heavily influenced by the need for the level of abstraction. How step by step do you need your program to be? Is the process of every piece of code or a more general idea of how the program work you need? When sitting as a studygroup who all has an average understanding of syntaxes, it is more convenient to incorporate the steps on code-like level, to get an idea of how to program is supposed to run. It is not completely step-by-step, but it is as detailed as a first draft of a superficial idea, can/needs to be. 

**What are the technical challenges for the two ideas and how are you going to address them?**
**For E.T (Emotional Translator)**
The technical challenge within this idea is probably to make the translation fit with the comment/post, or at least how to make this happen. At this moment we haven’t put further thought into how exactly it should be created, so that’s the challenge. We can either decide the translations should be random or specific to the comment/post that has appeared. 
**For P.S. (Personal Spindoctor)** 
The technical challenges (that we see at the moment) are perhaps making the user-input work and having the AI/spindoctor give a response - along with creating a functional submit-button. In terms of addressing these challenges, we are going look at the p5.js reference list (DOM: input & button), create and use a .JSON file (in terms of possible responses) and style the submit button with CSS.
It would be fun to incorporate the user input in the response somehow, to give a feeling of the response not being entirely random, but this seems to be a last thing to add, as it seems quite complex. How to do it would probably to use this.input somewhere in the response, yet that gives you the entire input which easily can make the response seem broken. 

