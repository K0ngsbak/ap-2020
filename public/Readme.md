## README FOR MINIX 1-10 WITHOUT SCREENSHOTS - FOR SCREENSHOT, LOOK AT THE REPOSITORY FOR EACH MINIX

## README MINIX1

Link:
https://k0ngsbak.gitlab.io/ap-2020/MyFirstSketch

My first independent coding experience was both frustrating when things did not go as planned; For instance, I struggled with coloring the sun yellow without coloring the grass yellow too. Later, I struggled with placing the roof of the house (the triangle). On the other hand, it was the best feeling when things did go as planned or when I realized how I could fix the problem. I boosted my confidence when things went as planned which I find quite nice.

When coding everything is “what happens if I do this...”; It is – for me - a learning by doing process. The similarities are that there are rules in coding as well as there are rules in writing, which makes it easier to understand, what you’re doing right and what you’re doing wrong.

At first, I saw programming as something way too technical and hardcore for me to try – I thought it was something only a few people could do. After reading the texts I realized that programming is just another type of artistic work. Which meant that programming is something one can use to try to change things in society. Something that one can use to express one’s opinions. Personally, I did not try to change anything with my first sketch; All I wanted to do was re-create a drawing that my little brother drew for me for my birthday last year (2019).



## README MINIX2 
Link:
www.k0ngsbak.gitlab.io/ap-2020/Minix2/

In my program I have used; the fill syntax, the vertex syntax, the image syntax,
the ellipse syntax, the rectangle syntax, the triangle syntax, the stroke
syntax, the no stroke syntax, the stroke weight syntax, the text syntax, the
text style syntax and the text size syntax. My program consists of a protest
emoji and a friendzone emoji and I put both into a context where one could use
these two emojis. The protest emoji is the emoji on the left side if the canvas
and it is supposed to be both gender-mutual and race-mutual because of the fact
that no matter who you are, you can have different opinions and therefore fight
for / protest against different things. The friendzone emoji is the emoji on
the right side of the canvas. The emoji is supposed to illustrate how two people
who care deeply for each other can have completely opposite feelings for each
other due to how one can want a friendship and how the other person might want
more. This is an alternative to the broken-heart emoji which in my opinion can
seem as the extreme; you are not heartbroken just because you’re crushing on
someone who doesn’t feel the same way. I would like to believe, that the friend-
zone emoji is a way of saying “I like you and it is okay that you do not feel
the same way”.



## README MINIX3
Link:
www.k0ngsbak.gitlab.io/ap-2020/public/Minix3/

My sketch is simply an (sarcastic) illustration of blinking tears in the front
with a bunch of words forming the short sentence “I’M FiNE” in the background
pretending to/ trying to load self-esteem. I know that the I in “I’M” is a
capital I and the I in “FiNE” is not – It was a mistake, now it is an artistic
choice along with the “sorry/needy” in the M.
Conceptual description
When first reading the task, I thought to myself: “I want to criticize an issue
that I have experienced myself”. Then I thought of a bunch of personal issues
such as; sexual harassment, low self-esteem, bullying, anxiety, depression etc.
Afterwards, I thought of what I used to believe was the solution to these issues
which I compared to what I now believe is the solution to these issues. At last
I settled on “low self-esteem” because of how I believe that my sketch could be
both a critique of society and a reminder to myself. So, what I used to believe
was that my low self-esteem would get better with time and all I had to do was
wait. Spoiler alert; Doing nothing but waiting isn’t working. This belief let to
a bunch of other issues, horrible relationships and a self-destructive way of
thinking because of how it didn’t get better, it got worse.
So, what I mainly want to express is that self-esteem is something you work for,
not wait for. Furthermore, I want to express how self-destructive low
self-esteem can be, how “I’m fine” doesn’t always hide the tears/how tears cuts
through your “I’m fine” and what “I’m fine’ often tries to hide.
Technical description
I wanted to make the teardrops rotate instead of blink, but after a full 6 hours
of not getting the rotation I wanted, I decided to go with the flow and keep the
blinking teardrops, which is (annoying) entertaining enough to be characterized
as a throbber due to how a throbber should maintain the concentration of the
user and try to make sure that the user do not feel like the she is “wasting”
her time waiting.


## README MINIX4
**SCRATCH) SELF HARM
IMPORTANT: Use the program before reading the readme.
IMPORTANT: View the program in a fully opened window – if at first the program will not appear, please re-size the window.**

RUNME:
Link to the program: https://k0ngsbak.gitlab.io/ap-2020/Minix4/

**Thoughts before coding:**
In our daily lives likes matter a lot; often a lot more than any of us would like (pun intended). We compare ourselves to each other and we get sad about the fact that we don't have that body, that smile, that eye color or are as happy as the person in the picture - we compare ourselves to everyone else, instead of learning to love who we are for what we are. My design addresses this problem by focusing on self-love as to why I wanted the camera to turn on when the user clicked “yes” to “Are you ready for some compliments?”, however due to lack of skills this was not possible as to why I changed it into the following: When the user clicks the “yes”-button, the background color will change and a bunch of compliments will appear around the “blinking” heart on the screen. This is supposed to illustrate the importance of loving oneself.

**Title and description:**
My program is called “(SCRATCH) SELF HARM” and it tries to illustrate the importance of self-love in a time, where we tend to criticize ourselves for what we are not instead of complimenting ourselves for who we are (I too am guilty of this). The program asks the user “are you ready for some compliments?” which of course is a “yes”/”no” kind of question, however it is not possible to answer the question with “no”. There are to (at least) reasons for this. The first reason being: if you want to answer this question with no, then you probably need the yes-outcome more than you think. The second reason being: the program tries to illustrate both the importance of self-love but also how technology impacts our lives a lot more than most of us notice. We tend to believe that we are in control of everything that happens online when often are not; You don’t even have the control to answer a simple yes/no question.
My hopes are that this last reason will make the user think about what he/she is in control of and not about what he/she is not in control of because of how I believe that you are in control of your own self-harm and your own self-love. In other words, I want the user to stop criticizing him-/herself for what he/she is not and start loving him-/herself for what he/she is.

**Describe your program and what you have used and learnt.**
My program consist of: an imperfect heart “blinking” in different colors, a “yes”-button that makes the background color change and a bunch of hidden compliments. The heart is imperfect because of how all of us have our own imperfections as to why the hearts serves as a reminder to the user. The heart is “blinking” in different colors to illustrate how different people are from each other as to why we should not compare ourselves to other people; and sadly, this is easier said than done.
In my program I have done quite a few things for the first time; I have created my own variables for the first time, I have used the window resize function for the first time, I have used “random” for the first time, I have used mousePressed for the first time and I have created a button for the first time. At this point, most of these are familiarities for most of the class, however I have been to “scared” to use them until now.
In my program, I have also used syntaxes, that I am familiar with such as: fill, text size, text style, text, framerate, noStroke, ellipse and triangle.

## README MINIX5
**Warning: I went with “low floor” this time due to lack of time and a bunch of personal issues that came out of nowhere**

Link to sketch: https://k0ngsbak.gitlab.io/ap-2020/Minix5/

**Background:** 
I have chosen to revisit my minix3 conceptually and revisit my minix4 technically. For my minix4 I worked with hidden text and a button that could change the color of the background. For my minix3 I wanted to criticize an issue that I have experienced myself as to why I thought about a bunch of personal issues such as; sexual harassment, low self-esteem, bullying, anxiety, depression etc. Afterwards, I settled on “low self-esteem” and thought of how I used to believe that my self-esteem would get better with time and all I had to do was wait. Spoiler alert; Doing nothing but waiting isn’t working. This belief let to a bunch of other issues, horrible relationships and a self-destructive way of thinking because of how it didn’t get better, it got worse. So, what I mainly want to express is that self-esteem is something you work for, not wait for. Furthermore, I want to express how self-destructive low self-esteem can be, how “I’m fine” doesn’t always hide the tears/how tears cuts through your “I’m fine” and what “I’m fine’ often tries to hide. 


**The concept**
So, for this minix I wanted to stay true to my message of how self-esteem is something you work for, not wait for – but in a more… Positive… way. I chose to design a crystal ball that could foresee a person’s future self-esteem with two possible outcomes; A positive outcome and a negative outcome depending on whether this person is going to work for a better self-esteem. 


**What changes and why**
Thinking of my minix3 I have changed the mood of the design and made it interactive. Further, I have changed a bunch of smaller and simple things such as the background color and use of shapes. Thinking of my minix4 I have changed the design of the button, but that’s about it. I chose to change the mood of the design based on how I wanted test the impact of the mood of the design. I chose to make the button blend in with the design compared to the button in my minix4 because of how I did not want the button to take away the focus due to how the message in this minix is much more important than the button. 


**Bonus**
I might re-do this minix in the future when I have more time and aren’t having an hour-long anxiety attack every day. If I am going to re-do this minix I think that I would have the crystal ball change color and not the whole background. Further, I would make an array of the negative words and an array of the positive words so that when the user clicks “this button” only one word will appear on each side of the crystal. I might even load a song to play in the background.  



## README MINIX6
RUNME link: https://k0ngsbak.gitlab.io/ap-2020/Minix6/

**Background//The process:**

So, when thinking of the task, I wanted program the simplest game I could, so I thought of making a gender-based tic-tac-toe. However, when I started programming, I didn’t feel exited about the idea and I quickly deleted the 60 lines that I had made so far and began to think of a new game. I then thought of how I used to play the traffic light game as a child, so I started programming a box changing colors from red, to yellow to green in a random order, but once again, I didn’t feel exited about the idea which lead to deleting the lines I had already made. T then talked to my little brother who is 9 years old btw and who spends a lot of his spare time on playing games, and he said to me: *“Well why don’t you just make the first game ever?”*. I felt excited about this idea because of how programmers and game developers have come so far since the first game. So: credits to my little brother for the idea.
I then began to research: What was the first game? If you google: *“first game created”* the game *“Pong”* would pop up. Note that it is not officially the first game to be created, but it is the first commercially successful. Also, Pong was created in June 1972. 


**Description of how the game/game objects work and how I programed the objects and their related attributes and methods in my game**
Warning, I’m being thorough because of how much I struggled with the game and how I would like to be able to understand what I have done when I look at the code again later in life. 😊 

The game consists of the following components: a player paddle, an ai paddle, a ball, a score system, a line in the middle of the game dividing the game into the player’s half of the screen and the ai’s half of the screen - and it works because of my nearly 200 lines of code. 

So, let me start with **the paddles**. The paddles in the original Pong-game were white, however I wanted to make my Pong-game differ from the original game as to why I made my paddles pink (I’ll come back to why in the “extend/connect”-question). The paddles work due to how I created a class called *class Paddle* in which I defined what it meant to be a paddle. A paddle is a pink rectangle with a stroke color like the color of the paddle. I could have used *noStroke()* since the color is the same, however this would have made the paddle look thinner, which was *not* my intention. Furthermore, the player is able to move the *playerPaddle* up and down but never off screen. Let me explain: The paddle cannot move off screen (any longer) due to my use of the if statements for *up()* and *down()* in my *class Paddle* (see lines 96-106 in the sketch file). The player is, however, able to move the playerPaddle up and down with the up-arrow and the down-arrow. This works because of how I used the *function keyPressed()* and the *function keyRelased()*. Both functions consists of an *if-statement*, an *else-if-statement* and *boolean values* (you can find these in lines 184-199).  
Now, let’s talk about **the AI**. I wanted to make the game a one-player game which meant that I would have to make an AI, however, the thought of making the *aiPaddle* almost scared me to death because of I believed that I was not able to make the AI due to my lack of coding-skills. Once again, I asked my wise younger brother who said to me: *“if you were a computer, how would you win the game?”*. I listened to my brother, and I thought to myself: *”If I was a computer, I would move up when the ball is above me and down when the ball is beneath me”*. And that is how the *aiPaddle* works. The aiPaddle constantly checks the location of the ball, which makes the computer a hard one to beat.

Next up, the ball. For the ball I made a class called *”class Ball”* which defines the ball as a white ellipse with *noStroke()* and a radius of 10px. Furthermore, if the ball hits the bottom or top of the screen it changes directions. If the ball goes to the end of the screen (left or right), a new round will begin, and the ball will start from the middle of the screen. At first, the ball didn’t bounce back when it hit a paddle, which meant I would have to find a way to make this happen. To do this, I had to make sure that it would bounce off both paddles, which meant I had to think about how the paddles differ from each other, and I found that the main difference Is that they are at different x-locations as to why the same hit direction would not work on both. What is meant by this is that the ball *“bounces”* back when the ball is at the same certain x-location as the paddle (see lines 147-168). 

Almost done, but let’s talk about the score system. For the score system I made the class called *class Score* and this was a bit more difficult that I thought it would be. Thinking of the score system, I had to remember that I need to include both the score of the player and the score of the AI as to why I made the variables called *let playerScore* and *let aiScore* which I called in the setup function along with the placement of the scores. At first, both score-counters were at the same location, and I couldn’t figure out why until I realized what a difference the +40 contra the -40 does (see lines 15-16); I had writen “-40” in both lines of code, which naturally gave them the same location. So, I changed the minus in line 16 to plus. Now that’s the location – what else? I had to think about when the score would change so I said to myself: “When the ball goes off the screen on the left, the computer gets a point and when the ball goes off screen on the right, the player gets a point.” In order to make this happen, I would have to implement this in the *class Ball* in lines 115-130). Also, the score is visible due to the *playerScore.display()* and the *aiScore.display()*. 

This is the last one! The halfway line only consist of two lines of code (lines 52-53); the stroke(255) which makes the straight line visible and the *line(width/2,0,width/2,height)” which determents the length of the line along with the placement of the line. The halfway line divides the game into the player’s half of the screen and the ai’s half of the screen, however it was at this point not clear to the user what half belonged to who (I had my brother try it out), as to why I implemented the “YOU” and the “COMP” in the background which again differs from the original pong-game along with the color of the paddles. 

**What are the characteristics of object-oriented programming and the wider implications of abstraction?**
One of the key characteristics of OOP (object-oriented programming) is that programs are divided into classes and objects as to why the code in OOP is organized around objects. An object is the main unit of OOP, additionally, an object is a component of the program that knows how to perform specific behaviors and how to interact with other objects. An object can be an image or something self-drawn (like I have done in my program). So, once you have an object this object can make something happen and/or interact with other objects (like how the ball in my program interacts with the paddles in my program). In order to create these objects, the programmer needs to have a class which means that these objects are defined in what is called classes. A class can be described as a template or a blueprint of an object and its capability of what this object can do. A class is very useful as it defines what it means to be a certain object such as a ball or a paddle. Lastly, abstractions. OOP is dependent on the abstractions which can be used to organize code.   


**Extend/connect your game project to wider digital culture context**
As I mentioned earlier, I wanted to make my game differ from the original Pong-game which I have done by making the paddles pink and by making it clear for the user what half of the screen belonged to who as to why I implemented the green “YOU” and the reed “COMP” in the background. I made the paddles pink because of how I was reminded of there is still a lot of ideologies and stereotypes such as "Women can't code". And I thought to myself: The first game was probably not programmed by a woman – and, obviously, I checked this assumption of mine. Pong was developed by Atari Inc and designed by Allan Alcorn. In which case, I was right. So, I wanted to leave my game with a hint of girlpower, and I thought that the best way to do this – without screaming feminism in the face of the user – was to make the paddles pink. Also, I wanted to illustrate how far we have come since Pong. 

*Reference: https://study.com/academy/lesson/oop-object-oriented-programming-objects-classes-interfaces.html*


## README MINIX7
**HAPPYBIRTHDAY** by Emma Marie Kongsbak Bertelsen


**RUNME link:** 
https://k0ngsbak.gitlab.io/ap-2020/Minix7/


**Link to repository:** https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public%2FMinix7 


**The rules in my program are:**
1) For each time the colored ellipses gets to the right side of the canvas, continue on the next line
2) For each time the gray-scale colored ellipses gets to the bottom of the canvas, continue on the next line
3) Keep drawing the “sad” text and the “happy” text 

**What's the role of rules and processes in your work?**
The rules I have set up are what creates the result which is supposed to represent my chaotic mind. I find the use of rules to be a great help in terms of setting up guidelines for the programmer, which I guess can come in handy when one has lots of lines of code compared to mine barely 50 lines of code.
 
**Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**
In contrast to Marius Watz I have chosen to work with a verbal experience of form and space when making generative art, however I have not made the verbal part as clear as I am used to in my previous mini exercises.
As one might notice, I have used random(); in my program which is why I find it important to focus on the chapter “Randomness” in “10 PRINT CHR$(205.5+RND(1)); : GOTO 10” by Nick Montford. In terms of randomness I believe that the text gives a great understanding of how nothing is truly random though it might look like it due to how there is a reason behind why something happens as to why it is not truly random yet it appears to be less controlled than it is. In other words, nothing is random because of how everything is pseudorandom.   
On that note, I personally find it hard to understand what an auto generator is however, I find the topic interesting in terms of control and instructions as to why I tried to make the program illustrate my (sometimes) chaotic mind thinking of how I like structure and typically follow a set of instructions in order to feel like I am in control of my own life.  


**Additional notes**
The program is called “happybirthday” because of my birthday in next week and the fact that I am no longer “in control” of the celebration of my birthday due to the coronavirus. The birthday-part is illustrated with in the program with the white “sad” and the red “happy” text.  




## README MINIX8 - huMANerrOR


*Made by Mira Bella Dyring Morsø & Emma Marie Kongsbak Bertelsen*


***My own link does not fully work for some reason, so here you have Miras link :)***

Link to program: https://miramorso.gitlab.io/ap-2020/MiniEx8/


Link to repository: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix8

**Title and conceptual description:** 

The title of our program is: “huMANerrOR”
With our sketch we want to comment on bullying, and how it has only become worse because of the access to internet that has highly increased since the origin of the World Wide Web. Also under the name: Cyber Bullying. With our program, we comment on how horrible bullying is - especially online where the bullies can hide behind their screens. Additionally, we comment on how AI are becoming a bigger and bigger part of our daily lives as to why AI’s might have a mind of their own in the future thinking of for instance Alexa, Siri and the Google Assistant - They do not have a mind of their own, however they are programmed to response to personal questions such as “Hey Alexa, what is your love-life like?” as well as questions with an exact answer such as “Hey Alexa, how is the weather outside?”. Thinking of how this could evolve, we would like to think that AIs (and the internet) would not be okay with cyber bullying (among others). On that note, our program is called huMANerrOR because of how humans are the ones doing the harm along with the fact that the computer can not execute because of how it does not understand the need to bully and harm other people, as to why the it describes the error to the user being: “human error” in order to make the user understand the error. The reason why our program is called “huMANerrOR” and not “human error” is up for interpretation. 

**Technical description:**

Our program is mostly visual and not very interactive. We decided to focus on the poetic message that the sketch is trying to give. 
We used a bunch of rect figures to create a big pixelated file with x’es as eyes, similar to the once you receive, when your website is unable to refresh. Within the figure is text that other than saying “ERROR” multiple places also says a bunch of statements evolving around the theme of bullying, which still is a big problem in the world we live it. All of these statements and messages are written down in a file called “error.json,” which we draw them from via our sketch file. Other than that we have used a few if statements to change the color of the text, and to activate a voice message saying: “ERROR.” These if statements are activated with both the position of the mouse but also whether or not the mouse is pressed. 
Additional note: Should one wish to revisit our program later to rewrite the code, one could consider to write the code likewise to the Queer-example from class 9.


**Our process:**
1.	We started out by figuring out what out concept would be [being the internet’s reaction (crashing) to cyberbullying if the internet had a personality and a mind of its own. This hopefully signalizes several things such as: the fact that bullying is not okay, that the internet doesn’t have to be a bad thing and that AI is becoming a bigger and bigger part of our daily lives]. 
2.	Then we tried to make the teletype package in atom work - it demanded a little more effort than the above, but we figured it out at last. 
3.	Next up was figuring out what we were going to program as to why we tried to show each other what we think of when the internet crashes. We decided to combine both of our suggestions which meant trying to figure out the best way to do this.
4.	Our first idea was to make the “face” of the internet with text - this was not going to work because of the minimal amount of text and the positions of the text. We then decided to draw the outline of the “face” with grey rectangles like the rectangles in the picture and fill the face with our statements instead. 
5.	After we had drawn the face in draw we discussed the possibility of making the face into a class which we tried to do of course, however we soon realized that it was a mistake and that our original plan with drawing it in the draw function() was better for us in this context. 
6.	Next, we had to figure out how to make our .json file. This turned out to be a struggle. 
7.	After a lot of tries and use of the json validator, we come to find out that a comma, was what made or sketch not work. And we have now made a successful connection between our sketch and our json file.
8.	At this point all we had to do was fill our “face” with the statements and sentences in our json file. Which made the code very long since almost every statement needed its own size and placement to fit. 
9.	After this we felt like the sketch was missing something and we decided to make the text change its fill depending on the x value of the mouse, but this wasn’t quite enough to make the result satisfying to us. 
10.	We decided to create a mp3 of a google translator say “error”, and make it play with every press of the mouse. This is to make “the computer” stop the user from going any further [into cyber bullying], there is an error that is impossible to get away from. No matter where you press you’ll get the same message: ERROR!


**Analysis:** 

In terms of the analysis of our program huMANerrOR we focus on the text The Aesthetics of Generative Code. The text mainly focus on senses and how we can only “appreciate generative code fully” if we “‘sense’ the code” so that we can “fully grasp what it is we are experiencing and to build an understanding of the code’s actions.”. When thinking of what it means to sense the code, we seek to describe the senses that the user uses when running our program; The senses are: The sense of touch (when the mouse is pressed and when the mouse is moved), the sense of sight (the “face” and the text written as the skin of the “face” along with the change of fill() when the mouse is moved) and the sense of hearing (the google translate voice saying “error”). Not all senses are used, however, we still find that our use of senses makes our program better and thereby allows the user to understand our program better. 
When thinking of poetry, the text states that: “poetry can neither be reduced to audible signs (the time of the ear) nor visible signs (the space of the eye) but is composed of language itself. This synthesis suggests that written and spoken forms work together to form a language that we appreciate as poetry. “. We would like to believe that thus our program is not a poem, it can be seen as poetic due to how our program expresses “emotions” (statements) through “art” and senses. Additionally, we believe that our program can be interpreted differently depending on the user’s experience with the program - we do, however, believe that the user receives the message because of our combination of written and spoken words.

**References:** 

*Finding statements for the text:*
https://onlinesense.org/bullying-quotes/


*Text used in analysis*
http://www.generativeart.com/on/cic/2000/ADEWARD.HTM 


*Trying to understand json with this example:*
https://editor.p5js.org/kimagosto/sketches/ryr40qHJ4




## README MINIX9 - GIFingEmotional

**NOTE: Personally, the program does not work in my standard browser (being Crome) however it works in Opera (for me). So, in other words - if you experience any trouble with using the program, try to open it in another browser :)**

Link to program: https://k0ngsbak.gitlab.io/ap-2020/Minix9/

Link to repository: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix9

#### Who are you collaborating with / which group?
**Group 4:** Sofie Berg Pedersen @SofieBP , Mira Bella Dyring Morsø @MiraMorso, Malene Storm Blomgren @maleneblomgren & Emma Marie Kongsbak Bertelsen @K0ngsbak  


***What is the program about? which API have you used and why?***
Sometimes it is hard to put words to your feelings, and explain to people how you feel. But a picture says more than a thousand words, right? So a gif most at least say more than 10 thousand words. Math. With our program we wanted to help the user express their feelings in an easy way. 
We live in a time where we use “emotions” everywhere on social media in order to react on the different posts we scroll through every day. Looking at Facebook for instance, one can react with both a like, a heart, a sad face, a surprised face (wow) and an angry face. Emotions become a bigger and bigger part of social media. 
When thinking of how our program works, it is important to show some light on the reason why the program doesn’t refresh when you click a new button, but also on the reason why the position of the GIFs are random. First, the reason why the program doesn’t refresh is because of how one often feels more than just one emotion and we would like our program to express this. Second, the reason why the position of GIFs are random is because of how we believe that one cannot control one’s emotions - so why should we control the position? [We did try to make the GIFs appear in one specific location/position but after a lot of struggles with this, we decided that is was for the best and we are quite happy with our final result].

***Can you describe and reflect on your process of making this mini exercise in terms of acquiring, processing, using and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the selected data? What are the power-relations in the chosen APIs? What is the significance of APIs in digital culture?***

It was hard to figure out what we wanted to make, knowing that there are so many API’s out there, so in a way it was hard to choose because of all the opportunities available. After looking through some different API’s we figured that we wanted to create something where emotions played a role, and it seemed fitting using gifs to represent those different types of emotions we wanted to display in our program. 
For our miniex we’re not visualizing data in a way as much as we’re simply putting the raw data in the program for everybody to see. The gifs or other data we could acquire from the site or not processed. Of course we’ve selected to focus on only a handful of categories inside their vast collection of gifs. So we’re kind of using their search function within their API, but not further tagging specific data to value and presenting or visualising them in any specific way. 
One way that platform providers at least control their data, is through APIs keys and/or tokens. Where they can see how much you’ve gotten on stop you if you’ve exceeded your limit. They can see what you are using it for how and what exactly you are using. Take for example Google that will allow you to request up to a hundred pictures a day from their picture API, yet as you’ve reached the limit, you simply can’t request anymore. This is one of the power-relations at play. Asymmetrical exchange can definitely, be called into this as you as a user of Google provide the data for them, for example when uploading a picture to your blog, but when you try to access the data they’ve mined from you, they will either but limit to your access or simply deny if the data are of the category that is sold or simply of an intern database for their own use. 
At first we wanted every new gif to replace the older one, but decided against it. We decided that a trail of gifs fitting different moods would show, how complicated human feelings are. We cannot only feel one thing at a time, and it takes time to figure out what exactly you are feeling in tough situations. 

***Try to formulate a question in relation to web APIs or querying/parsing processes that you want to investigate further if you have more time.***

Can you really use APIs in the way that nag/net.art generator does, to put pressure on larger platforms/firms to gain further accessibility and/or transparency? Won’t larger platforms always disregard the smaller uproars against closed, un-transparent data-mining? They gain large sums of selling data and APIs they won’t just give that up. 

### References: 
We used this sketch to figure out how to place our gifs on the canvas

https://gitlab.com/Freyaw/miniex1?fbclid=IwAR1ywpOJ1vu5u12c9fJ9fPDgBox06h0AvdZj3ZCdjJHadCfOf0O9NpWjMwA


We looked at Herborg, Linnea, Line and Lauras’ sketch to get inspiration to create the style of  our buttons, and changed them a bit to fit our own idea.

https://gitlab.com/linesdmoller/ap2020/-/tree/master/public/MiniX9


We used this link to download the font we wanted for our headline 

https://www.dafont.com/blackbird.font


We used Shiffman's’ video to understand how to use API data from giphy - and as our main inspiration

https://www.youtube.com/watch?v=mj8_w11MvH8

## README MINIX10 - Individual
Link to repository: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix10

**Process of making the flowchart**
For this minix I have chosen to make a flowchart of my Pong-game (minix6: https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix6) for several reasons. First, this was the most technically complex one for me to work with. Secondly, I probably would have liked to have a flowchart when making the game, because it turned out to be quite complex to work with quite fast – and I struggled a lot. Funny thing is, when making the flowchart this time around, I thought that it would be easy to do when I could just look at the code and make it based on the code. That didn’t work out for me since I quickly realized that when I made the code, I jumped back and forth – so I couldn’t just look at the code line by line because that was not how I made the game in the first place. That meant closing my computer and finding pen and paper instead – in other words; starting from scratch. I asked myself: “How to you want to make the flowchart?” and I wrote down: “A help from my future self to my past self”. In order to do so, I tried to think of how I made it first time around – but this did not work for me either. Then what? Well, I guess third time is a charm. When I didn’t succeed, I decided that I would pretend like I didn’t know how to make the game and try to make the flowchart as if I was going to make it for the first time. I think this worked out great since this way of thinking was how I made the final flowchart. 
Personally, this minix have taught me (among others) that I prefer to make the flowchart before the code. 


**What is the value of the individual and the group flowchart that you have produced?**
I have approached my individual flowchart the same way that we approached the group flowchart by making the flowcharts make sense to myself/ourselves. The value of flowcharts for me is based on the fact that it is a huge help in terms of communicating; both the conceptual and technical aspects of a program – especially when working in a group since it helps us reach a common understanding of our ideas. It is, however, important for me to state that I do not see flowcharts as some fort of final walkthrough of an idea, but rather as an overview of the basics; a blueprint some might say. On that note, I think that it is important to look at the flowchart again during the process of making the code of the program in terms of how it can help you stay on track – and maybe help you out if you get lost. In other words; there is (to me) a value in flowcharts that I did not realize before this minix – it is not just something you make in the beginning and then never looks at again, it is a way of understanding an idea and getting more concrete first time around – at least to me it is. 

## README MINIX10 - Group
#### Who are you collaborating with / which group?
**Group 4:** Sofie Berg Pedersen @SofieBP , Mira Bella Dyring Morsø @MiraMorso, Malene Storm Blomgren @maleneblomgren & Emma Marie Kongsbak Bertelsen @K0ngsbak

**Our ideas**
*ET (Emotional Translator):*
The overall idea for this is to make a look similar to the translate feature we see on Facebook. On Facebook you have the option to get comments and posts translated into your own language if the text is in a foreign language to you. It gives the user the ability to understand all content shared, even if you don’t understand the language it’s written in. We wanted to play with the idea of the facade we put on when sharing our lives on social media, and create an “Emotional Translator,” that will give the user the option to understand what the author of the post or comment is really thinking or feeling.


*PS (Personal Spindoctor):*
The overall idea for this is to make an somewhat nihilistic AI that can/will help the user achieve the best (and possibly the most honest) post to post on a social media platform. We wanted to comment on how many users on social media post their best selves and often not their honest selves. We find that social media accounts are often putting up a façade as to why we acknowledge why some might criticize the lack of “deepness” in some posts on social media. 


**What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?**
Some things are easier to express when using the correct term, for example array, preload, setup, draw and so forth, yet people who are unfamiliar with the language will not understand the words. It takes some time, to convert your idea into common tongue, so that it can be a proper communication device, that allows a diverse team of people to corporate. 
At another level of complexity is also heavily influenced by the need for the level of abstraction. How step by step do you need your program to be? Is the process of every piece of code or a more general idea of how the program work you need? When sitting as a studygroup who all has an average understanding of syntaxes, it is more convenient to incorporate the steps on code-like level, to get an idea of how to program is supposed to run. It is not completely step-by-step, but it is as detailed as a first draft of a superficial idea, can/needs to be. 

**What are the technical challenges for the two ideas and how are you going to address them?**
**For E.T (Emotional Translator)**
The technical challenge within this idea is probably to make the translation fit with the comment/post, or at least how to make this happen. At this moment we haven’t put further thought into how exactly it should be created, so that’s the challenge. We can either decide the translations should be random or specific to the comment/post that has appeared. 
**For P.S. (Personal Spindoctor)** 
The technical challenges (that we see at the moment) are perhaps making the user-input work and having the AI/spindoctor give a response - along with creating a functional submit-button. In terms of addressing these challenges, we are going look at the p5.js reference list (DOM: input & button), create and use a .JSON file (in terms of possible responses) and style the submit button with CSS.
It would be fun to incorporate the user input in the response somehow, to give a feeling of the response not being entirely random, but this seems to be a last thing to add, as it seems quite complex. How to do it would probably to use this.input somewhere in the response, yet that gives you the entire input which easily can make the response seem broken. 

