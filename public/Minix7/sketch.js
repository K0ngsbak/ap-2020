//AUTO GENERATOR
  //RULES
  //1) For each time the colored ellipses gets to the right side of the canvas, continue on the next line
  //2) For each time the gray-scale colored ellipses gets to the bottom of the canvas, continue on the next line
  //3) Keep drawing the “sad” text and the “happy” text

    let x = 0;
    let y = 0;
    let spacing = 10;

    function setup() {
      createCanvas(600,600);
      background(0);
    }

    function draw() {
      noStroke();
      if (random(1) <0.25) {  //probabilty
        //ellipse for positive thoughts
        fill(random(255),0,random(255));
        ellipse(y,x,2+spacing,2+spacing);
      } else  if (random(1)<0.50){
        //ellipse for negative thoughts
        fill(random(255));
        ellipse(x,y,2+spacing,2+spacing);
      }
      x+=10;
      if (x > width) {
        x = 0;
        y += spacing;
      }
      //Sad with a for loop
      for (let x=0;x<=600;x=x+50){
        textSize(14);
        fill(255,10);
        text("sad",x,random(y));
      }
      //Happy with a for loop
      for (let y=0;y<=600;y=y+75){
        fill(255,0,0,7);
        text("happy",y,random(x));
      }
    }
