**HAPPYBIRTHDAY** by Emma Marie Kongsbak Bertelsen


**RUNME link:** 
https://k0ngsbak.gitlab.io/ap-2020/Minix7/


**Link to repository:** https://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public%2FMinix7 


Screenshot: ![Screenshot](happybirthday1.png)
Screenshot: ![Screenshot](happybirthday2.png)
Screenshot: ![Screenshot](happybirthday3.png)

**The rules in my program are:**
1) For each time the colored ellipses gets to the right side of the canvas, continue on the next line
2) For each time the gray-scale colored ellipses gets to the bottom of the canvas, continue on the next line
3) Keep drawing the “sad” text and the “happy” text 

**What's the role of rules and processes in your work?**
The rules I have set up are what creates the result which is supposed to represent my chaotic mind. I find the use of rules to be a great help in terms of setting up guidelines for the programmer, which I guess can come in handy when one has lots of lines of code compared to mine barely 50 lines of code.
 
**Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?**
In contrast to Marius Watz I have chosen to work with a verbal experience of form and space when making generative art, however I have not made the verbal part as clear as I am used to in my previous mini exercises.
As one might notice, I have used random(); in my program which is why I find it important to focus on the chapter “Randomness” in “10 PRINT CHR$(205.5+RND(1)); : GOTO 10” by Nick Montford. In terms of randomness I believe that the text gives a great understanding of how nothing is truly random though it might look like it due to how there is a reason behind why something happens as to why it is not truly random yet it appears to be less controlled than it is. In other words, nothing is random because of how everything is pseudorandom.   
On that note, I personally find it hard to understand what an auto generator is however, I find the topic interesting in terms of control and instructions as to why I tried to make the program illustrate my (sometimes) chaotic mind thinking of how I like structure and typically follow a set of instructions in order to feel like I am in control of my own life.  


**Additional notes**
The program is called “happybirthday” because of my birthday in next week and the fact that I am no longer “in control” of the celebration of my birthday due to the coronavirus. The birthday-part is illustrated with in the program with the white “sad” and the red “happy” text.   
