var errors;
let voice;

function setup(){
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function preload(){
  errors = loadJSON("error.JSON");
  voice = loadSound('voice.mp3')
}


function draw(){
  fill(70);
  noStroke();

  // voice memo saying: "error" whenever mouse is pressed
  if (mouseIsPressed) {
    voice.play();
  }

  //makes the fill of the statements change with the placement of the mouseX
  if (mouseX<200) {
    fill (100);
  } else if (mouseX<400) {
    fill(200);
  } else if (mouseX<600) {
    fill(250);
  }

  // all the json statements put in with different sizes and placements to fill out the figure
  textSize(10);
  text(errors.statements[0],115,100);
  text(errors.statements[15],115,268);
  text(errors.statements[11],249,240);
  text(errors.statements[15],475,268);

  textSize(11);
  text(errors.statements[8],115,157);
  text(errors.statements[6],120,197);
  text(errors.statements[14],115,257);
  text(errors.statements[7],117,310);
  text(errors.statements[14],475,257);


  textSize(13);
  text(errors.statements[16],115,145);
  text(errors.statements[12],249,210);
  text(errors.statements[12],249,252);
  text(errors.statements[12],115,392);
  text(errors.statements[12],275,393);
  text(errors.statements[10],435,393);

  textSize(14);
  text(errors.statements[15],115,345);
  text(errors.statements[14],115,360);

  textSize(15.4);
  text(errors.statements[5],114,376);

  textSize(17.5);
  text(errors.statements[3],115,215);
  text(errors.statements[3],115,230);
  text(errors.statements[3],115,245);
  text(errors.statements[14],433,315);
  text(errors.statements[2],385,331);
  text(errors.statements[1],249,227);
  text(errors.statements[1],249,270);
  text(errors.statements[3],114,330);
  text(errors.statements[3],183,330);
  text(errors.statements[15],433,332);
  text(errors.statements[3],475,215);
  text(errors.statements[3],475,230);
  text(errors.statements[3],475,245);
  text(errors.statements[3],250,358);
  text(errors.statements[3],317,358);

  textSize(19.5);
  text(errors.statements[2],480,377);

  textSize(25);
  text(errors.statements[17],430,355);

  textSize(27.5);
  text(errors.statements[11],115,420);

  textSize(32.5);
  text(errors.statements[9],115,297);

  textSize(33.5);
  text(errors.statements[13],115,186);

  textSize(36);
  text(errors.statements[3],115,130);


  //the 'face' of the error
  // pixels
  push();
  scale(0.45);
  translate(200,100);
  fill(128);
  noStroke();
  //Shape of the face
  rect(50,50,300,50);
  rect(400,50,300,50);
  rect(350,100,50,100)
  rect(700,100,50,100);
  rect(350,200,650,50);
  rect(1000,250,50,600);
  rect(0,100,50,750);
  rect(0,850,1050,50);
  //eye on the left
  rect(200,350,50,50);
  rect(300,350,50,50);
  rect(250,400,50,50);
  rect(200,450,50,50);
  rect(300,450,50,50);
  //eye on the right
  rect(700,350,50,50);
  rect(800,350,50,50);
  rect(750,400,50,50);
  rect(700,450,50,50);
  rect(800,450,50,50);
  //mouth
  rect(250,650,100,50);
  rect(350,600,300,50);
  rect(650,650,100,50);
  rect(750,700,100,50);
  pop();
}

errors = stopthebullying
