My sketch is simply an (sarcastic) illustration of blinking tears in the front 
with a bunch of words forming the short sentence “I’M FiNE” in the background 
pretending to/ trying to load self-esteem. I know that the I in “I’M” is a 
capital I and the I in “FiNE” is not – It was a mistake, now it is an artistic
choice along with the “sorry/needy” in the M.


**Conceptual description**
When first reading the task, I thought to myself: “I want to criticize an issue 
that I have experienced myself”. Then I thought of a bunch of personal issues 
such as; sexual harassment, low self-esteem, bullying, anxiety, depression etc. 
Afterwards, I thought of what I used to believe was the solution to these issues
which I compared to what I now believe is the solution to these issues. At last
I settled on “low self-esteem” because of how I believe that my sketch could be 
both a critique of society and a reminder to myself. So, what I used to believe
was that my low self-esteem would get better with time and all I had to do was
wait. Spoiler alert; Doing nothing but waiting isn’t working. This belief let to
a bunch of other issues, horrible relationships and a self-destructive way of 
thinking because of how it didn’t get better, it got worse. 
So, what I mainly want to express is that self-esteem is something you work for,
not wait for. Furthermore, I want to express how self-destructive low 
self-esteem can be, how “I’m fine” doesn’t always hide the tears/how tears cuts
through your “I’m fine” and what “I’m fine’ often tries to hide. 


**Technical description**
I wanted to make the teardrops rotate instead of blink, but after a full 6 hours
of not getting the rotation I wanted, I decided to go with the flow and keep the
blinking teardrops, which is (annoying) entertaining enough to be characterized
as a throbber due to how a throbber should maintain the concentration of the
user and try to make sure that the user do not feel like the she is “wasting” 
her time waiting. 


**Runme:**
Link to sketch: 
https://k0ngsbak.gitlab.io/ap-2020/Minix3/

Link to repository: 
http://gitlab.com/K0ngsbak/ap-2020/-/tree/master/public/Minix3

Screenshot:
![Screenshot](loadingselfesteem.png)

