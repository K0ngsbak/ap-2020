function setup() {
  // put setup code here
  createCanvas(500,500);
  print("hello world");
}
function draw() {
  // put drawing code here
  //creating background
  background(47);
  fill(255,255,255,70);
  textSize(14);
  //I in I'm
  text('self-destructive',110,60);
  text('barely haning on',110,170);
  text('l',157,75);
  text('o',155,90);
  text('n',155,105);
  text('e',155,120);
  text('l',157,135);
  text('y',155,150);
  // ' in I'm
  text('w',245,39);
  text('e',245,48);
  text('a',245,58);
  text('k',245,70);
  // m in I'm
  text('d',290,60);
  text('a',290,75);
  text('m',290,90);
  text('a',290,105);
  text('g',290,120);
  text('i',293,135);
  text('n',290,150);
  text('g',290,165);

  text('s',300,60);
  text('o',310,70);
  text('r',320,80);
  text('r',330,90);
  text('y',340,100);

  text('n',380,60);
  text('e',370,70);
  text('e',360,80);
  text('d',350,90);

  text('h',390,60);
  text('o',390,75);
  text('r',392,90);
  text('r',392,105);
  text('i',392,120);
  text('b',390,135);
  text('l',392,150);
  text('e',390,165);

  //F in fine
  text('confused',50,360);
  text('dysfuntional',50,300);
  text('w',35,300);
  text('o',35,315);
  text('r',35,330);
  text('t',35,345);
  text('h',35,360);
  text('l',35,375);
  text('e',35,390);
  text('s',35,405);
  text('s',35,420);
  //I in fine
  text('d',170,300);
  text('e',170,315);
  text('p',170,330);
  text('r',170,345);
  text('e',170,360);
  text('s',170,375);
  text('s',170,390);
  text('e',170,405);
  text('d',170,420);
  //N in fine
  text('s',230,300);
  text('a',230,315);
  text('d',230,330);
  text('-',230,345);
  text('a',230,360);
  text('n',230,375);
  text('g',230,390);
  text('r',230,405);
  text('y',230,419);

  text('h',245,300);
  text('e',249,312);
  text('a',253,324);
  text('r',257,336);
  text('t',261,348);
  text('b',265,360);
  text('r',269,372);
  text('o',273,384);
  text('k',277,396);
  text('e',281,408);
  text('n',285,420);

  text('e',300,298);
  text('m',300,310);
  text('p',300,322);
  text('t',300,337);
  text('y',300,348);
  text('-',300,360);
  text('a',300,372);
  text('l',300,384);
  text('o',300,396);
  text('n',300,408);
  text('e',300,421);
  //E in fine
  text('sick of crying',370,300);
  text('tired of trying',370,360);
  text('inside i´m dying',370,420);
  text('n',355,300);
  text('o',355,315);
  text('t',357,330);
  text('e',355,350);
  text('n',355,363);
  text('o',355,376);
  text('u',355,389);
  text('g',355,403);
  text('h',355,420);
  drawElements();
}
function drawElements(){
  //Loading self-esteem text
  fill(255,0,0,150);
  textSize(18);
  text('Loading self-esteem...',180,230);
  //blinking teardrops
  frameRate(12);
  let num=3;
  push();
  translate(CENTER);
  let cir=360/num*(frameCount%num);
  rotate(radians(cir));
  //Drawing teardrop #1
  fill(255,255,255);
  noStroke();
  triangle(275,85,250,30,225,85);
  ellipse(250,90,50,50);
  //Drawing teardrop #2 & #6
  fill(0,170,255);
  triangle(410,165,385,110,360,165);
  ellipse(385,170,50,50);
  triangle(150,165,125,110,100,165);
  ellipse(125,170,50,50);
  //Drawing teardrop nr #3 & #5
  fill(0,0,255);
  triangle(410,305,385,250,360,305);
  ellipse(385,310,50,50);
  triangle(150,305,125,250,100,305);
  ellipse(125,310,50,50);
  //Drawing teardrop #4
  fill(0,0,150);
  triangle(275,405,250,350,225,405);
  ellipse(250,410,50,50);
  pop();
}
