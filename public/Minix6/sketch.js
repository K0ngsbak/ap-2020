//This is a one person pong-game.
//The playerPaddle represents the player, and the aiPaddle represents the computer.
let playerPaddle;
let aiPaddle;
let ball;
let playerScore;
let aiScore;

function setup(){
  createCanvas(624,351);
  print('hello world');
  playerPaddle=new Paddle(26);
  aiPaddle=new Paddle(width-48);
  ball=new Ball();
  playerScore = new Score(width/2-40);
  aiScore=new Score(width/2+40);
}

function draw(){
  background(0);
  //Making sure that the user knows who he/she is on the screen
textSize(50);
  stroke(0,255,0);
  fill(0,255,0);
  text("YOU",150,175);
  stroke(255,0,0);
  fill(255,0,0);
  text("COMP",465,175);
  textSize(11);
  noStroke();
  fill(255,255,255);
  text("Use the UP-arrow on your keyboard to move your paddle up &",156,340);
  text("the DOWN-arrow on your keyboard to move your paddle down",470,340);

  playerPaddle.display();
  aiPaddle.display();

  playerPaddle.update();
  aiPaddle.update();

  //make the paddle move
  if (playerPaddle.isUp) {
    playerPaddle.up();
  } else if (playerPaddle.isDown) {
    playerPaddle.down();
  }

  processAI();

  ball.update(playerScore,aiScore);
  ball.display();

  ball.hasHitPlayer(playerPaddle);
  ball.hasHitAi(aiPaddle);

  //Drawing the white halfway in the middle of the screen
  stroke(255);
  line(width/2,0,width/2,height);

  playerScore.display();
  aiScore.display();
}
//Makeing the computer "come to life"
function processAI(){
  let middleOfPaddle=aiPaddle.y+aiPaddle.height/2;

  if (middleOfPaddle>ball.y) {
    aiPaddle.isUp=true;
    aiPaddle.isDown=false;
  } else {
    aiPaddle.isDown=true;
    aiPaddle.isUp=false;
  }
}

//Creating class for paddle + using display which is going to be called so that my paddles show on the canvas
class Paddle{
  constructor(x){
    this.x=x;
    this.y=height/2;
    this.height=80;
    this.width=20;
    //Letting the player controle the direction of the paddle part 1 #boolean values
    this.isUp=false;
    this.isDown=false;
  }
  update(){
    //Making the paddle move
    if (this.isUp) {
      this.up();
    }else if (this.isDown) {
      this.down();
    }
  }
  display(){
    stroke(255,0,175);
    fill(255,0,175);
    rect(this.x,this.y,20,80)
  }
  //Since the paddles need to go one of two directions (up or down), I've created a 'function' in the class called up() and down()
  //Making sure that the paddle can not go off screen
  up(){
    if (this.y>0) {
      this.y-=2;
      }
  }

  down(){
    if (this.y<height-this.height) {
    this.y+=2;
    }
  }
}
//Creating the class for the ball
class Ball{
  constructor(){
    this.r=10;
    this.reset();
  }
  update(playerScore,aiScore){
    //if the ball hits the top or bottom of the screen change direction
    if (this.y<this.r||this.y>height-this.r) {
      this.ySpeed=-this.ySpeed;
    }
    //if the ball goes to the end of the scren, restart
    if (this.x<this.r){
      aiScore.increment();
        this.reset();
    }else if (this.x>width+this.r) {
      playerScore.increment();
        this.reset();
    }
    this.x+=this.xSpeed;
    this.y+=this.ySpeed;
  }
  reset(){
    this.x=width/2;
    this.y=height/2;
    this.xSpeed=random(3,4);
    //the following code determins if the ball is going left or right
    let isLeft=random(1)>.5;
    if(isLeft){
      this.xSpeed=-this.xSpeed;
    }
    this.ySpeed=random(-3,3);
  }
  display(){
    noStroke();
    fill(255);
    ellipse(this.x,this.y,this.r*2,this.r*2);
  }
  //Making sure that the ball bounces back if the ball hits the playerPaddle or the aiPaddle
  //hasHitPlayer checks to see whether the ball has hit the player using the player's cordinates
  hasHitPlayer(player){
    if(this.x-this.r<=player.x+player.width && this.x>player.x){
      if (this.isSameHeight(player)) {
        this.xSpeed=-this.xSpeed;
      }
    }
  }
  //checks to see wether the ball has hit the ai using the ai's cordinates
  hasHitAi(ai){
    if (this.x+this.r>=ai.x&&this.x<=ai.x+ai.width) {
      if(this.isSameHeight(ai)) {
        this.xSpeed=-this.xSpeed;
      }
    }
  }
  // can be used by both the hasHitPlayer and the hasHitAi
  isSameHeight(player){
    return this.y >= player.y&&this.y<=player.y+player.height;
  }
}

//creating the class score for score system
class Score{
  constructor(x){
    this.x=x;
    this.score=0;
  }
  display(){
    textSize(50);
    textAlign(CENTER);
      text(this.score,this.x,60);
  }
  increment(){
    this.score++;
  }
}
//Letting the player controle the direction of the paddle part 2#Up arrow and down arrow
function keyPressed(){
  if(keyCode===UP_ARROW){ //up-arrow
    playerPaddle.isUp=true;
  }else if (keyCode===DOWN_ARROW) { //down-arrow
      playerPaddle.isDown=true;
  }
}
function keyReleased(){
  if (keyCode==UP_ARROW){
    playerPaddle.isUp=false;
  }else if (keyCode==DOWN_ARROW) {
    playerPaddle.isDown=false;
  }
}
