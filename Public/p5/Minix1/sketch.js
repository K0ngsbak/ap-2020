function setup() {
  // put setup code here
  createCanvas(500,500);
  print("hello world");
    //integer RGBA notaion
  background(color(0,255,255));
  let c = color (255,255,0); //Define color 'c'
  fill(c); // Use color variable 'c' as fill color
  noStroke(); // Don't draw a stroke aorund shapes
  ellipse(450,50,80,80); //Draw left circle
  // Using only one value with color()
// generates a grayscale value.
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(50, 50, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(70, 50, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(90, 50, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(110, 50, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(80, 60, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(70, 36, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(100, 40, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(150, 150, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(130, 140, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(100, 150, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(120, 160, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(250, 75, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(275, 65, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(290, 75, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(320, 75, 50, 50); // Draw right circle
c = color(255,255,255); // Update 'c' with grayscale value
fill(c); // Use updated 'c' as fill color
ellipse(285, 85, 50, 50); // Draw right circle
}
function draw() {
  // put drawing code here
  //integer RGB notation
  fill('rgb(0,255,0)');
  //Draw a rectangle at location (30,20) with a width and height of 55
  rect(0, 400, 500, 100);
  // put drawing code here
  //integer RGB notation
  fill('rgb(255,0,0)');
  //Draw a rectangle at location (30,20) with a width and height of 55
  rect(220, 330, 100, 90);
  // put drawing code here
  //integer RGB notation
  fill('rgb(0,0,0)');
  //Draw a rectangle at location (30,20) with a width and height of 55
  rect(270, 375, 30, 45);
  triangle(200,345,265,290,340,345);
  // Using only one value with color()
  // generates a grayscale value.
  c = color(255,255,255); // Update 'c' with grayscale value
  fill(c); // Use updated 'c' as fill color
  ellipse(292, 400, 5, 5); // Draw right circle
}
